package com.basiclab.common.network.enums;

/**
 * Created by jiangwenping on 2017/7/3.
 * 网络类型
 */
public enum NetTypeEnum {
    HTTP,
    WEBSOCKET,
    TCP,
    UDP
    ;
}
