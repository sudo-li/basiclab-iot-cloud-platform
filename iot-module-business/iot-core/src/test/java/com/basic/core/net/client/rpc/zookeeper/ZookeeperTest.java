package com.basic.core.net.client.rpc.zookeeper;

import com.basiclab.iot.core.service.config.GameServerConfigService;
import com.basiclab.iot.core.service.rpc.server.RpcServerRegisterConfig;
import com.basic.core.TestStartUp;
import com.basic.common.network.config.GameServerConfig;
import com.basic.common.network.util.BeanUtil;
import com.basiclab.iot.core.bootstrap.manager.LocalMananger;
import com.basiclab.iot.core.service.rpc.client.ZookeeperRpcServiceDiscovery;
import com.basiclab.iot.core.service.rpc.server.SdRpcServiceProvider;
import com.basiclab.iot.core.service.rpc.server.zookeeper.ZooKeeperNodeBoEnum;
import com.basiclab.iot.core.service.rpc.server.zookeeper.ZooKeeperNodeInfo;
import com.basiclab.iot.core.service.rpc.server.zookeeper.ZookeeperRpcServiceRegistry;

import java.util.List;

/**
 * Created by jiangwenping on 17/3/30.
 */
public class ZookeeperTest {

    private ZookeeperRpcServiceRegistry zookeeperRpcServiceRegistry;

    private ZookeeperRpcServiceDiscovery zookeeperRpcServiceDiscovery;

    public static void main(String[] args) throws Exception {
        ZookeeperTest zookeeperTest = new ZookeeperTest();
        zookeeperTest.init();
        zookeeperTest.test();
        zookeeperTest.close();
    }
    public void init() throws Exception {
        TestStartUp.startUpWithSpring();
        zookeeperRpcServiceRegistry = (ZookeeperRpcServiceRegistry) BeanUtil.getBean("zookeeperRpcServiceRegistry");
        zookeeperRpcServiceDiscovery = (ZookeeperRpcServiceDiscovery) BeanUtil.getBean("zookeeperRpcServiceDiscovery");
    }

    public void test() {
        zookeeperRpcServiceRegistry.registerZooKeeper();
        GameServerConfigService gameServerConfigService = LocalMananger.getInstance().getLocalSpringServiceManager().getGameServerConfigService();
        RpcServerRegisterConfig rpcServerRegisterConfig = gameServerConfigService.getRpcServerRegisterConfig();
        SdRpcServiceProvider sdRpcServiceProvider = rpcServerRegisterConfig.getSdRpcServiceProvider();
        GameServerConfig gameServerConfig = gameServerConfigService.getGameServerConfig();
        String serverId = gameServerConfig.getServerId();
        String host = gameServerConfig.getRpcBindIp();
        String ports = gameServerConfig.getRpcPorts();
        ZooKeeperNodeInfo zooKeeperNodeInfo = new ZooKeeperNodeInfo(ZooKeeperNodeBoEnum.WORLD, serverId, host, ports);

        zookeeperRpcServiceRegistry.register(zooKeeperNodeInfo.getZooKeeperNodeBoEnum().getRootPath(),zooKeeperNodeInfo.getNodePath(), zooKeeperNodeInfo.serialize());
        zookeeperRpcServiceDiscovery.discovery(ZooKeeperNodeBoEnum.WORLD);
        List<ZooKeeperNodeInfo> dataList = zookeeperRpcServiceDiscovery.getNodeList(ZooKeeperNodeBoEnum.WORLD);
        System.out.println(dataList);
    }

    public void close() throws Exception {
//        zookeeperRpcServiceRegistry.deleteNode(zookeeperRpcServiceRegistry.getZk(), ZooKeeperNodeBoEnum.WORLD.getRootPath());
//        zookeeperRpcServiceRegistry.deleteNode(zookeeperRpcServiceRegistry.getZk(), ZooKeeperNodeBoEnum.GAME.getRootPath());
//        zookeeperRpcServiceRegistry.deleteNode(zookeeperRpcServiceRegistry.getZk(), ZooKeeperNodeBoEnum.DB.getRootPath());

        zookeeperRpcServiceRegistry.shutdown();
        zookeeperRpcServiceDiscovery.stop();
        LocalMananger.getInstance().getLocalSpringServiceManager().stop();
    }
}
