package com.basic.core.common.util;

import com.basic.common.network.util.RandomStringGenerator;

/**
 * Created by jiangwenping on 17/2/6.
 */
public final class RandomStringGeneratorTest {
    public static void main(String[] args) {
       String string =  new RandomStringGenerator().generateRandomString(10);
        System.out.println(string);
    }
}
