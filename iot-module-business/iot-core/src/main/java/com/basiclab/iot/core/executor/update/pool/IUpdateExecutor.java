package com.basiclab.iot.core.executor.update.pool;

import com.basiclab.iot.core.executor.update.entity.IUpdate;
import com.basiclab.iot.core.executor.update.thread.dispatch.DispatchThread;

/**
 * Created by jwp on 2017/2/23.
 * 执行一个update
 */
public interface IUpdateExecutor {
    void executorUpdate(DispatchThread dispatchThread, IUpdate iUpdate, boolean firstFlag, int updateExcutorIndex);

    void startup();

    void shutdown();
}
