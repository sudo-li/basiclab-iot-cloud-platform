package com.basiclab.iot.core.executor.event.impl.listener;

import com.basiclab.iot.core.executor.common.utils.Constants;
import com.basiclab.iot.core.executor.event.EventParam;
import com.basiclab.iot.core.executor.event.common.IEvent;
import com.basiclab.iot.core.executor.event.impl.event.FinishedEvent;
import com.basiclab.iot.core.executor.update.entity.IUpdate;
import com.basiclab.iot.core.executor.update.service.UpdateService;
import com.basiclab.iot.core.executor.update.thread.dispatch.DispatchThread;

/**
 * Created by jiangwenping on 17/1/11.
 */
public class DispatchFinishEventListener extends FinishEventListener {

    private final UpdateService updateService;
    private final DispatchThread dispatchThread;

    public DispatchFinishEventListener(DispatchThread dispatchThread, UpdateService updateService) {
        this.dispatchThread = dispatchThread;
        this.updateService = updateService;
    }

    @Override
    public void fireEvent(IEvent event) {
        super.fireEvent(event);
        //提交更新服务器 执行完成调度
        EventParam[] eventParams = event.getParams();
        IUpdate iUpdate = (IUpdate) eventParams[0].getT();
        FinishedEvent finishedEvent = new FinishedEvent(Constants.EventTypeConstans.finishedEventType, iUpdate.getUpdateId(), event.getParams());
        this.updateService.addFinishedEvent(finishedEvent);
    }

}
