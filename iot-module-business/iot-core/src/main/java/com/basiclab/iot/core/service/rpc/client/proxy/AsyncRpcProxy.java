package com.basiclab.iot.core.service.rpc.client.proxy;

import com.basiclab.iot.core.bootstrap.manager.LocalMananger;
import com.basiclab.iot.core.service.net.tcp.RpcRequest;
import com.basic.iot.core.service.rpc.client.*;
import com.basiclab.iot.core.service.rpc.client.net.RpcClient;
import com.basiclab.iot.core.service.rpc.client.*;

/**
 * Created by jwp on 2017/3/9.
 */


public class AsyncRpcProxy<T> implements IAsyncRpcProxy{

    private final Class<T> clazz;

    public AsyncRpcProxy(Class<T> clazz) {
        this.clazz = clazz;
    }

    @Override
    public RPCFuture call(String funcName, Object... args) {
        RpcContextHolderObject rpcContextHolderObject = RpcContextHolder.getContext();
        RpcClientConnectService rpcClientConnectService = LocalMananger.getInstance().getLocalSpringServicerAfterManager().getRpcClientConnectService();
        AbstractRpcConnectManager abstractRpcConnectManager = rpcClientConnectService.getRpcConnectMannger(rpcContextHolderObject.getBoEnum());
        RpcClient rpcClient = abstractRpcConnectManager.chooseClient(rpcContextHolderObject.getServerId());
        RpcRequestFactory rpcRequestFactory = LocalMananger.getInstance().getLocalSpringBeanManager().getRequestFactory();
        RpcRequest request = rpcRequestFactory.createRequest(this.clazz.getName(), funcName, args);
        RPCFuture rpcFuture = rpcClient.sendRequest(request);
        return rpcFuture;
    }


}
