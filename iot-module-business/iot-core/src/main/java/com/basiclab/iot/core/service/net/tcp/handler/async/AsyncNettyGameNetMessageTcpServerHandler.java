package com.basiclab.iot.core.service.net.tcp.handler.async;

import com.basiclab.iot.core.bootstrap.manager.LocalMananger;
import com.basic.common.network.constant.Loggers;
import com.basiclab.iot.core.logic.net.NetMessageProcessLogic;
import com.basiclab.iot.core.service.lookup.NetTcpSessionLoopUpService;
import com.basiclab.iot.core.service.message.AbstractNetProtoBufMessage;
import com.basiclab.iot.core.service.net.tcp.MessageAttributeEnum;
import com.basiclab.iot.core.service.net.tcp.handler.AbstractGameNetMessageTcpServerHandler;
import com.basiclab.iot.core.service.net.tcp.session.NettyTcpSession;
import com.basiclab.iot.core.service.net.tcp.session.builder.NettyTcpSessionBuilder;
import io.netty.channel.Channel;
import io.netty.channel.ChannelHandlerContext;
import org.slf4j.Logger;

/**
 * Created by jiangwenping on 2017/5/22.
 * 使用AsyncNettyTcpHandlerService的handler
 *
 *  不会进行session的游戏内循环经查，断网后直接删除缓存，抛出掉线事件
 */
public class AsyncNettyGameNetMessageTcpServerHandler extends AbstractGameNetMessageTcpServerHandler {
    public static Logger logger = Loggers.sessionLogger;


    @Override
    public void channelRead(ChannelHandlerContext ctx, Object msg) throws Exception {
        AbstractNetProtoBufMessage netMessage = (AbstractNetProtoBufMessage) msg;
        Channel channel = ctx.channel();
        //直接进行处理

        //装配session
        NetTcpSessionLoopUpService netTcpSessionLoopUpService = LocalMananger.getInstance().getLocalSpringServiceManager().getNetTcpSessionLoopUpService();
        long sessonId = channel.attr(NettyTcpSessionBuilder.channel_session_id).get();
        NettyTcpSession nettySession = (NettyTcpSession) netTcpSessionLoopUpService.lookup(sessonId);
        if (nettySession == null) {
            logger.error("tcp netsession null channelId is:" + channel.id().asLongText());
            //已经丢失session， 停止处理
            return;
        }
        netMessage.setAttribute(MessageAttributeEnum.DISPATCH_SESSION, nettySession);

        //进行处理
        NetMessageProcessLogic netMessageProcessLogic = LocalMananger.getInstance().getLocalSpringBeanManager().getNetMessageProcessLogic();
        netMessageProcessLogic.processMessage(netMessage, nettySession);

    }
    @Override
    public   void addUpdateSession(NettyTcpSession nettyTcpSession){

    }
}
