//
// Source code recreated from a .class file by IntelliJ IDEA
// (powered by Fernflower decompiler)
//

package com.basiclab.iot.core.time;

public interface ExpirationListener<E> {
    void expired(E var1);
}
