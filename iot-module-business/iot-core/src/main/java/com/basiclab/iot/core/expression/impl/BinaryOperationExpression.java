//
// Source code recreated from a .class file by IntelliJ IDEA
// (powered by Fernflower decompiler)
//

package com.basiclab.iot.core.expression.impl;


import com.basiclab.iot.core.expression.Expression;

public abstract class BinaryOperationExpression implements Expression {
    private static final long serialVersionUID = -176241089133898830L;
    protected Expression left;
    protected Expression right;

    public BinaryOperationExpression() {
    }

    public Expression getLeft() {
        return this.left;
    }

    public void setLeft(Expression left) {
        this.left = left;
    }

    public Expression getRight() {
        return this.right;
    }

    public void setRight(Expression right) {
        this.right = right;
    }

    public abstract int getPriority();
}
