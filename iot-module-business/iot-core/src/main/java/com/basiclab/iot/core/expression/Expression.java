//
// Source code recreated from a .class file by IntelliJ IDEA
// (powered by Fernflower decompiler)
//

package com.basiclab.iot.core.expression;

import java.io.Serializable;

public interface Expression extends Serializable {
    long getValue(long var1);
}
