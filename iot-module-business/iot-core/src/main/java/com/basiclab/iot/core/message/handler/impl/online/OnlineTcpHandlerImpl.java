package com.basiclab.iot.core.message.handler.impl.online;

import com.basiclab.iot.core.bootstrap.manager.LocalMananger;
import com.basic.common.network.annotation.MessageCommandAnnotation;
import com.basic.common.network.constant.Loggers;
import com.basiclab.iot.core.logic.player.GamePlayer;
import com.basiclab.iot.core.message.handler.AbstractMessageHandler;
import com.basiclab.iot.core.message.logic.tcp.online.client.OnlineLoginClientTcpMessage;
import com.basiclab.iot.core.message.logic.tcp.online.server.OnlineLoginServerTcpMessage;
import com.basiclab.iot.core.service.lookup.GamePlayerLoopUpService;
import com.basiclab.iot.core.service.message.AbstractNetMessage;
import com.basiclab.iot.core.service.message.command.MessageCommandIndex;
import com.basiclab.iot.core.service.net.tcp.MessageAttributeEnum;
import com.basiclab.iot.core.service.net.tcp.session.NettyTcpSession;

import java.util.concurrent.atomic.AtomicLong;

/**
 * Created by jiangwenping on 17/2/21.
 */
public class OnlineTcpHandlerImpl extends AbstractMessageHandler {

    private final AtomicLong id = new AtomicLong();

    @MessageCommandAnnotation(command = MessageCommandIndex.ONLINE_LOGIN_TCP_CLIENT_MESSAGE)
    public AbstractNetMessage handleOnlineLoginClientTcpMessage(OnlineLoginClientTcpMessage message) throws Exception {
        OnlineLoginServerTcpMessage onlineLoginServerTcpMessage = new OnlineLoginServerTcpMessage();
        long playerId = 6666 + id.incrementAndGet();
        int tocken = 333;
        onlineLoginServerTcpMessage.setPlayerId(playerId);
        onlineLoginServerTcpMessage.setTocken(tocken);
        if (Loggers.sessionLogger.isDebugEnabled()) {
            Loggers.sessionLogger.debug( "playerId " + playerId + "tocken " + tocken + "login");
        }
        NettyTcpSession clientSesion = (NettyTcpSession) message.getAttribute(MessageAttributeEnum.DISPATCH_SESSION);
        GamePlayer gamePlayer = new GamePlayer(clientSesion.getNettyTcpNetMessageSender(), playerId, tocken);
        GamePlayerLoopUpService gamePlayerLoopUpService = LocalMananger.getInstance().getLocalSpringServiceManager().getGamePlayerLoopUpService();
        gamePlayerLoopUpService.addT(gamePlayer);
        return onlineLoginServerTcpMessage;
    }
}
