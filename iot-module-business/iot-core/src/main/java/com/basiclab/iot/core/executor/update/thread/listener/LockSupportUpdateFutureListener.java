package com.basiclab.iot.core.executor.update.thread.listener;

import com.basiclab.iot.core.future.ITaskFuture;
import com.basiclab.iot.core.future.ITaskFutureListener;
import com.basiclab.iot.core.executor.common.utils.Constants;
import com.basiclab.iot.core.executor.common.utils.Loggers;
import com.basiclab.iot.core.executor.event.EventParam;
import com.basiclab.iot.core.executor.event.impl.event.UpdateEvent;
import com.basiclab.iot.core.executor.update.cache.UpdateEventCacheService;
import com.basiclab.iot.core.executor.update.entity.IUpdate;
import com.basiclab.iot.core.executor.update.thread.update.LockSupportUpdateFuture;

import java.util.concurrent.locks.LockSupport;

/**
 * Created by jiangwenping on 17/1/18.
 */
public class LockSupportUpdateFutureListener implements ITaskFutureListener {

    @Override
    public void operationComplete(ITaskFuture iTaskFuture) throws Exception {
        if(Loggers.gameExecutorUtil.isDebugEnabled()){
            IUpdate iUpdate = (IUpdate) iTaskFuture.get();
            Loggers.gameExecutorUtil.debug("update complete event id " + iUpdate.getUpdateId());
        }
        LockSupportUpdateFuture lockSupportUpdateFuture = (LockSupportUpdateFuture) iTaskFuture;
        IUpdate iUpdate = (IUpdate) iTaskFuture.get();
        //事件总线增加更新完成通知
        EventParam<IUpdate> params = new EventParam<IUpdate>(iUpdate);
//        UpdateEvent event = new UpdateEvent(Constants.EventTypeConstans.updateEventType, iUpdate.getUpdateId(), params);
        UpdateEvent updateEvent = UpdateEventCacheService.createUpdateEvent();
        updateEvent.setEventType(Constants.EventTypeConstans.updateEventType);
        updateEvent.setId(iUpdate.getUpdateId());
        updateEvent.setParams(params);
        updateEvent.setUpdateAliveFlag(iUpdate.isActive());
        lockSupportUpdateFuture.getDispatchThread().addUpdateEvent(updateEvent);
        //解锁
        LockSupport.unpark(lockSupportUpdateFuture.getDispatchThread());
    }
}
