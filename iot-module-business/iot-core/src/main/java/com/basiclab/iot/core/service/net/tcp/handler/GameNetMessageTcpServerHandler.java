package com.basiclab.iot.core.service.net.tcp.handler;

import com.basiclab.iot.core.bootstrap.manager.LocalMananger;
import com.basiclab.iot.core.executor.common.utils.Constants;
import com.basiclab.iot.core.executor.event.CycleEvent;
import com.basiclab.iot.core.executor.event.EventParam;
import com.basiclab.iot.core.executor.update.service.UpdateService;
import com.basiclab.iot.core.service.message.AbstractNetProtoBufMessage;
import com.basiclab.iot.core.service.net.tcp.pipeline.IServerPipeLine;
import com.basiclab.iot.core.service.net.tcp.session.NettyTcpSession;
import com.basiclab.iot.core.service.update.NettyTcpSerssionUpdate;
import io.netty.channel.ChannelHandlerContext;

/**
 * Created by jiangwenping on 17/2/7.
 * tcp协议处理handler
 */
public class GameNetMessageTcpServerHandler extends AbstractGameNetMessageTcpServerHandler {

    @Override
    public void channelRead(ChannelHandlerContext ctx, Object msg) throws Exception {
        AbstractNetProtoBufMessage netMessage = (AbstractNetProtoBufMessage) msg;
        //获取管道
        IServerPipeLine iServerPipeLine = LocalMananger.getInstance().getLocalSpringBeanManager().getDefaultTcpServerPipeLine();
        iServerPipeLine.dispatchAction(ctx.channel(), netMessage);
    }

    @Override
    public void addUpdateSession(NettyTcpSession nettyTcpSession){
        //加入到updateservice
        UpdateService updateService = LocalMananger.getInstance().getUpdateService();
        NettyTcpSerssionUpdate nettyTcpSerssionUpdate = new NettyTcpSerssionUpdate(nettyTcpSession);;
        EventParam<NettyTcpSerssionUpdate> param = new EventParam<NettyTcpSerssionUpdate>(nettyTcpSerssionUpdate);
        CycleEvent cycleEvent = new CycleEvent(Constants.EventTypeConstans.readyCreateEventType, nettyTcpSerssionUpdate.getUpdateId(), param);
        updateService.addReadyCreateEvent(cycleEvent);
    }

}
