package com.basiclab.iot.core.service.lookup;

import com.basiclab.iot.core.bootstrap.manager.LocalMananger;
import com.basic.common.network.config.GameServerConfig;
import com.basic.common.network.constant.Loggers;
import com.basic.common.network.constant.ServiceName;
import com.basiclab.iot.core.service.IService;
import com.basiclab.iot.core.service.config.GameServerConfigService;
import com.basiclab.iot.core.service.limit.AtomicLimitNumber;
import com.basiclab.iot.core.service.net.tcp.session.NettySession;
import com.basiclab.iot.core.service.net.tcp.session.NettyTcpSession;
import org.slf4j.Logger;
import org.springframework.stereotype.Service;

import java.util.concurrent.ConcurrentHashMap;

/**
 * Created by jwp on 2017/2/10.
 * session提供服务
 */
@Service
public class NetTcpSessionLoopUpService implements IChannleLookUpService, IService {

    protected static final Logger log = Loggers.serverStatusStatistics;

    protected ConcurrentHashMap<Long, NettySession> sessions = new ConcurrentHashMap<Long, NettySession>();

    private AtomicLimitNumber atomicLimitNumber;

    @Override
    public NettySession lookup(long channelId) {
        return sessions.get(channelId);
    }

    @Override
    public boolean addNettySession(NettyTcpSession nettyTcpSession) {

        if (log.isDebugEnabled()) {
            log.debug("add nettySesioin " + nettyTcpSession.getChannel().id().asLongText() + " sessionId " + nettyTcpSession.getSessionId());
        }
        long current = atomicLimitNumber.increment();
        if(!checkMaxNumber(current)){
            atomicLimitNumber.decrement();
            return false;
        }
        sessions.put(nettyTcpSession.getSessionId(), nettyTcpSession);
        return true;
    }

    public boolean checkMaxNumber(long current) {
        GameServerConfigService gameServerConfigService = LocalMananger.getInstance().getLocalSpringServiceManager().getGameServerConfigService();
        GameServerConfig gameServerConfig = gameServerConfigService.getGameServerConfig();
        int maxNumber = gameServerConfig.getMaxTcpSessionNumber();
        return current <= maxNumber;
    }

    @Override
    public boolean removeNettySession(NettyTcpSession nettyTcpSession) {
        if (log.isDebugEnabled()) {
            log.debug("remove nettySesioin " + nettyTcpSession.getChannel().id().asLongText() + " sessionId " + nettyTcpSession.getSessionId());
        }
        atomicLimitNumber.decrement();
        return sessions.remove(nettyTcpSession.getSessionId()) != null;
    }

    @Override
    public String getId() {
        return ServiceName.NetTcpSessionLoopUpService;
    }

    @Override
    public void startup() throws Exception {
        atomicLimitNumber = new AtomicLimitNumber();
    }

    @Override
    public void shutdown() throws Exception {
        sessions.clear();
    }

    public ConcurrentHashMap<Long, NettySession> getSessions() {
        return sessions;
    }

    public void setSessions(ConcurrentHashMap<Long, NettySession> sessions) {
        this.sessions = sessions;
    }
}
