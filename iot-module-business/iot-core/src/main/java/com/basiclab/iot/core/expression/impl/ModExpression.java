//
// Source code recreated from a .class file by IntelliJ IDEA
// (powered by Fernflower decompiler)
//

package com.basiclab.iot.core.expression.impl;

public class ModExpression extends BinaryOperationExpression {
    private static final long serialVersionUID = 128643126283846133L;

    public ModExpression() {
    }

    @Override
    public long getValue(long key) {
        return this.left.getValue(key) % this.right.getValue(key);
    }

    @Override
    public int getPriority() {
        return 2;
    }
}
